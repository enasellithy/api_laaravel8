<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([ 'namespace' => 'App\Http\Controllers\Api', 'prefix' => 'v1'],function (){
    Route::group(['namespace' => 'Auth', 'middleware' => 'api'], function (){
        Route::post('login','AuthController@login');
    });

    Route::group(['middleware' => 'checkApi'], function (){
        Route::post('upload_file','UploadFileController@uploadfile');
        Route::resource('menu','Data\DataController')
            ->except('edit');
    });
});
