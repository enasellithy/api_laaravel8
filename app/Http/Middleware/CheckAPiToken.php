<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAPiToken
{
    public function handle(Request $request, Closure $next)
    {
        if(Auth::guard('api')->user()){
            $this->auth = true;
            $this->user = Auth::guard("api")->user();
            $this->id = Auth::guard('api')->user()->id;
            return $next($request);
        } else{
            return response()->json([
                'status' => false,
                'message' => 'unauthorized',
                'data' => [],
            ]);
        }
    }
}
