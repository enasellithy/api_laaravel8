<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemOptionResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'Name' => $this->Name,
            'price' => $this->price,
            'MaxQty' => $this->MaxQty,
        ];
    }
}
