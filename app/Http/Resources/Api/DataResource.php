<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class DataResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ItemName' => $this->ItemName,
            'ItemDescription' => $this->ItemDescription,
            'price' => $this->price,
            'user_id' => $this->user->name,
            'option' => ItemOptionResource::collection($this->option),
        ];
    }
}
