<?php

namespace App\Http\Controllers\Api\Data;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\DataResource;
use App\Solid\Repository\DataRepository;
use Illuminate\Http\Request;

class DataController extends Controller
{
    private $data;

    public function __construct(DataRepository $dataRepo)
    {
        $this->data = $dataRepo;
    }

    public function index()
    {
        $data = DataResource::collection($this->data->getAllData());
        return response()->json([
            'status' => true,
            'data' => $data,
            'message' => 'Done'
        ],200);
    }

    public function show($id)
    {
        $data = new DataResource($this->data->getById($id));
        return response()->json([
            'status' => true,
            'data' => $data,
            'message' => 'Done'
        ],200);
    }

    public function destroy($id)
    {
        $this->data->destroy($id);
        return response()->json([
            'status' => true,
            'data' => [],
            'message' => 'Done'
        ],200);
    }
}
