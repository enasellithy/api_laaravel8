<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\LoginRequest;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(LoginRequest $r)
    {
        if (auth()->attempt(['email' => $r->email, 'password' => $r->password])) {
            $data['token'] = auth()->user()->createToken('LaravelAuthApp')->accessToken;
            return response()->json([
                'status' => true,
                'data' => $data,
                'message' => 'Done'
            ],200);
        }else{
            return response()->json([
                'status' => false,
                'data' => [],
                'message' => 'error',
            ],200);
        }
    }
}
