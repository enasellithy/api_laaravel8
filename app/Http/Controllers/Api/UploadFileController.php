<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Solid\Repository\DataRepository;
use Illuminate\Http\Request;
use App\Http\Requests\Api\UploadFileRequest;

class UploadFileController extends Controller
{
    private $data;

    public function __construct(DataRepository $dataRepo)
    {
        $this->data = $dataRepo;
    }

    public function uploadfile(UploadFileRequest $r)
    {
        $this->data->uploadfile($r->all());
        return response()->json([
            'status' => true,
            'data' => [],
            'message' => 'Done'
        ],200);
    }
}
