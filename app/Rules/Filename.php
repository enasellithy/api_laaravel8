<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Filename implements Rule
{
    public function passes($attribute, $value)
    {
        if (!($value instanceof UploadedFile) || !$value->isValid()) {
            return false;
        }
        return preg_match($this->regex, $value->getClientOriginalName()) > 0;
    }

    public function message()
    {
        return 'The :attribute name is invalid.';
    }
}
