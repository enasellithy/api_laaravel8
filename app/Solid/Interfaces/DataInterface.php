<?php


namespace App\Solid\Interfaces;


interface DataInterface
{
    public function uploadFile(array $data);

    public function insertData(array $data);

    public function getAllData();

    public function getById($id);

    public function destroy($id);
}
