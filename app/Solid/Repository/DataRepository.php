<?php


namespace App\Solid\Repository;


use App\Models\ItemOption;
use App\Models\MenuItem;
use App\Solid\Interfaces\DataInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class DataRepository implements DataInterface
{
    public function uploadFile(array $data)
    {
        $file = $data['file'];
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        if($extension != 'json'){
            return response()->json([
                'status' => false,
                'data' => [],
                'message' => 'error'
            ],200);
        }
        elseif($filename != 'a.json'){
            return response()->json([
                'status' => false,
                'data' => [],
                'message' => 'error'
            ],200);
        }
        else{
            return $this->insertData($data);
        }
    }

    public function insertData(array $data)
    {
        $getData = File::get($data['file']->getRealPath());
        $json = json_decode($getData,true);
        foreach ($json['MenuItems'] as $i) {
            $menu = MenuItem::create([
                'ItemName' => $i['ItemName'],
                'ItemDescription' => $i['ItemDescription'],
                'price' => $i['Price'],
                'user_id' => Auth::guard('api')->user()->id,
            ]);
            foreach($i['ItemOptions'] as $sub){
                ItemOption::create([
                    'Name' => $sub['Name'],
                    'price' => $sub['Price'],
                    'MaxQty' => $sub['MaxQty'],
                    'menu_item_id' => $menu->id,
                ]);
            }
        }
    }

    public function getAllData()
    {
        return MenuItem::where('user_id',Auth::guard('api')->user()->id)
            ->with('option')->paginate(50);
    }

    public function getById($id)
    {
        return MenuItem::findOrFail($id);
    }

    public function destroy($id)
    {
        $menu = MenuItem::findOrFail($id);
        if($menu->user_id == Auth::guard('api')->user()->id){
            foreach ($menu->option as $i){
                $item = ItemOption::findOrFail($i->id)->delete();
            }
           $menu->delete($id);
        }
    }
}
