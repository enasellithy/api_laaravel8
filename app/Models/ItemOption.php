<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemOption extends Model
{
    use HasFactory;

    protected $table = 'item_options';

    protected $fillable = [
        'id',
        'Name',
        'price',
        'MaxQty',
        'menu_item_id',
    ];

    public $timestamps = false;

    public function menu_item(){
        return $this->belongsTo(MenuItem::class,'.menu_item_id');
    }
}
